module.exports = {
  semi: false, // 行末用分号
  singleQuote: true, // 使用单引号
  arrowParens: 'always', // 箭头函数参数使用括号;avoid-单一参数省略；always-不可省略括号
  trailingComma: 'none', // 对象或数组末尾行加逗号;none-不加;es5-加;always-不可省略
  jsxBracketSameLine: true, // jsxBracketSameLine
  htmlWhitespaceSensitivity: 'ignore', // strict-所有空格都将认定为是有意义的;ignore-所有空格都将认定为是无意义的
  printWidth: 120, // 行宽度字符数
}
