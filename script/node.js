console.log('hello node')

const fs = require('fs')
const path = require('path')

// 读写文件
const readAndWriteFile = () => {
  let html = fs.readFileSync(path.resolve(__dirname, '../node/demo.md'), 'utf-8')
  html = html.replace(/##/g, '>>>**<<<')
  fs.writeFileSync(path.resolve(__dirname, '../node/text.md'), html)
}
readAndWriteFile()

/**
 * 文件遍历方法
 * @param filePath 需要遍历的文件路径
 */
const fileDisplay = (filePath) => {
  //根据文件路径读取文件，返回文件列表
  fs.readdir(filePath, (err, files) => {
    if (err) {
      console.warn(err)
    } else {
      //遍历读取到的文件列表
      files.forEach((filename) => {
        //获取当前文件的绝对路径
        const filedir = path.join(filePath, filename)
        //根据文件路径获取文件信息，返回一个fs.Stats对象
        fs.stat(filedir, (eror, stats) => {
          if (eror) {
            console.warn('获取文件stats失败')
          } else {
            const isFile = stats.isFile() // 是文件
            const isDir = stats.isDirectory() // 是文件夹
            if (isFile) {
              console.log(filedir)
              const text = fs.readFileSync(filedir, 'utf-8')
              console.log(text)
            }
            if (isDir) {
              fileDisplay(filedir) // 递归，如果是文件夹，就继续遍历该文件夹下面的文件
            }
          }
        })
      })
    }
  })
}

fileDisplay(path.resolve(__dirname, '../node'))

// 文件重命名
const fsRename = (oldFile, newFile) => {
  fs.rename(oldFile, newFile, () => {
    console.log('rename')
  })
}

fsRename(path.resolve(__dirname, '../node', './rename.js'), path.resolve(__dirname, '../node', './Rename.js'))
