// eslint-disable-next-line
import { createRouter, createWebHistory, createWebHashHistory } from 'vue-router'
// Home
import Home from '@/views/Home.vue'
import HomeIndex from '@/views/home/HomeIndex.vue'
import GridIndex from '@/views/home/grid/Index.vue'
const HomeGrid = () => import('@/views/home/grid/HomeGrid.vue')
const GridAddRemove = () => import('@/views/home/grid/GridAddRemove.vue')
const CustomGrid = () => import('@/views/home/grid/CustomGrid.vue')

const Test = () => import('@/views/Test.vue')
const Setup = () => import('@/views/Setup.vue')
// Element
const ElementIndex = () => import('@/views/element/ElementIndex.vue')
const ElementHome = () => import('@/views/element/ElementHome.vue')
const ElementForm = () => import('@/views/element/ElementForm.vue')
const ElementTree = () => import('@/views/element/ElementTree.vue')
const ElementUpload = () => import('@/views/element/ElementUpload.vue')
const ElementTable = () => import('@/views/element/ElementTable.vue')
const ElementPages = () => import('@/views/element/ElementPages.vue')
const ElementCarousel = () => import('@/views/element/ElementCarousel.vue')
const ElementCalendar = () => import('@/views/element/ElementCalendar.vue')
const ElementTransfer = () => import('@/views/element/ElementTransfer.vue')
// test
const TestIndex = () => import('@/views/element/test/TestIndex.vue')
const TestDrag = () => import('@/views/element/test/TestDrag.vue')

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    redirect: '/home/index',
    isHomePage: true,
    children: [
      {
        path: '/home/index',
        name: 'HomeIndex',
        component: HomeIndex,
        desc: '首页',
        icon: 'el-icon-office-building'
      },
      {
        path: '/home/grid',
        name: 'GridIndex',
        component: GridIndex,
        desc: '栅格系统',
        icon: 'el-icon-office-building',
        redirect: '/home/grid/1',
        children: [
          {
            path: '/home/grid/1',
            name: 'HomeGrid',
            component: HomeGrid,
            desc: '栅格-1'
          },
          {
            path: '/home/grid/add-remove',
            name: 'GridAddRemove',
            component: GridAddRemove,
            desc: '增加-删除'
          },
          {
            path: '/home/grid/add-custom',
            name: 'CustomGrid',
            component: CustomGrid,
            desc: '自定义栅格'
          }
        ]
      }
    ]
  },
  {
    path: '/test',
    name: 'Test',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: Test
  },
  {
    path: '/setup',
    name: 'Setup',
    component: Setup
  },
  {
    path: '/element',
    name: 'ElementIndex',
    component: ElementIndex,
    redirect: '/element/home',
    desc: 'element-plus',
    isElementPlus: true,
    children: [
      {
        path: '/element/home',
        name: 'ElementHome',
        component: ElementHome,
        desc: '基础组件',
        icon: 'el-icon-office-building'
      },
      {
        path: '/element/form',
        name: 'ElementForm',
        component: ElementForm,
        desc: 'Form表单',
        icon: 'el-icon-message'
      },
      {
        path: '/element/tree',
        name: 'ElementTree',
        component: ElementTree,
        desc: '树形控件',
        icon: 'el-icon-grape'
      },
      {
        path: '/element/upload',
        name: 'ElementUpload',
        component: ElementUpload,
        desc: '上传组件',
        icon: 'el-icon-upload2'
      },
      {
        path: '/element/table',
        name: 'ElementTable',
        component: ElementTable,
        desc: 'Table表格',
        icon: 'el-icon-s-grid'
      },
      {
        path: '/element/pages',
        name: 'ElementPages',
        component: ElementPages,
        desc: 'pages分页',
        icon: 'el-icon-folder-opened'
      },
      {
        path: '/element/carousel',
        name: 'ElementCarousel',
        component: ElementCarousel,
        desc: '走马灯',
        icon: 'el-icon-picture'
      },
      {
        path: '/element/calendar',
        name: 'ElementCalendar',
        component: ElementCalendar,
        desc: '日历',
        icon: 'el-icon-date'
      },
      {
        path: '/element/transfer',
        name: 'ElementTransfer',
        component: ElementTransfer,
        desc: '穿梭框',
        icon: 'el-icon-d-arrow-right'
      },
      {
        path: '/element/test',
        name: 'TestIndex',
        component: TestIndex,
        desc: 'test',
        icon: 'el-icon-d-arrow-right',
        redirect: '/element/test/drag',
        children: [
          {
            path: '/element/test/drag',
            name: 'TestDrag',
            component: TestDrag,
            desc: '拖拽测试'
          }
        ]
      }
    ]
  }
]

const router = createRouter({
  // history: createWebHistory(process.env.BASE_URL), // HTML5模式路由
  // history: createWebHashHistory(process.env.BASE_URL), // 哈希路由
  history: createWebHistory(window.__POWERED_BY_QIANKUN__ ? '/vue3-element' : '/'),
  routes
})

export default router
